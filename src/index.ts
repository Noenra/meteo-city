const btn = document.getElementById('recherche');
let ville = document.getElementById('ville');

let city_name = document.getElementById('city');
let weatherText = document.getElementById('weather')
let weather = document.getElementById('sky');
let température_min = document.getElementById('temp_min');
let température = document.getElementById('temp')
let température_max = document.getElementById('temp_max');
let windSpeed = document.getElementById('windSp');
let windDeg = document.getElementById('windDeg');
let humitee = document.getElementById('humidity');
//@ts-ignore
weather.src = `https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Design_plat_Neutre_fort.svg/1024px-Design_plat_Neutre_fort.svg.png`;

btn.addEventListener('click', () => {
    //@ts-ignore
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${ville.value}&units=metric&appid=ba9bf36b49bda07fa20ba69e4665b3a5`)
    .then(resp => resp.json())

    .then(data => {
        weatherText.innerHTML = `${data.weather[0].main}`
        //@ts-ignore
        weather.src = `https://openweathermap.org/img/w/${data.weather[0].icon}.png`;
        //@ts-ignore
        weather.alt = `${data.weather[0].main}`;
        city_name.innerHTML = "Ville: " + data.name + ".";
        température_min.innerHTML = `Température min: ${data.main.temp_min}°C.`;
        température.innerHTML = `Température ressenti: ${data.main.temp}°C.`
        température_max.innerHTML = `Tempétrature max: ${data.main.temp_max}°C.`;
        windSpeed.innerHTML = "Vitesse du vent: " + data.wind.speed + ".";
        windDeg.innerHTML = "Direction du vent: " + data.wind.deg + ".";
        humitee.innerHTML = "Taux d'humitée: " + data.main.humidity + "%.";

        // console.log("Ville: " + data.name + ".");
        // console.log("Ciel: " + data.weather[0].main + ".");
        // console.log("Température min: " + data.main.temp_min + "°C.");
        // console.log("Tempétrature max: " + data.main.temp_max + "°C.");    
        // console.log("Vitesse du vent: " + data.wind.speed, ", Direction du vent: " + data.wind.deg + ".");
        // console.log("Taux d'humitée: " + data.main.humidity);
        console.log(data);
    })

    .catch(error => {
        console.log(error);
    })
});